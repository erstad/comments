<?php
/**
 * Created by PhpStorm.
 * User: henrik
 * Date: 16.11.15
 * Time: 11:32
 */

namespace Comments\Controller;

use Comments\Model\CommentsTable;
use Zend\Http\PhpEnvironment\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Comments\Model\Comment;
use Comments\Form\CommentForm;

class CommentsController extends AbstractActionController
{
    /** @var  CommentsTable */
    protected $commentsTable;

    public function indexAction()
    {
        return new ViewModel(
            array(
                'comments' => $this->getCommentsTable()->fetchCommentsTree(),
            )
        );
    }

    public function replyAction()
    {

        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            $id = 0;
        }

        try {
            $comment = $this->getCommentsTable()->getComment($id);
        } catch (\Exception $ex) {
            $comment = new Comment();
        }

        $form = new CommentForm();

        //
        $form->bind($comment);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $comment->exchangeArray($form->getData());
                $comment->parent = $id;
                $this->getCommentsTable()->saveComment($comment);

                return $this->redirect()->toRoute('comments');
            }
        }
        return array(
            'id'   => $id,
            'form' => $form
        );
    }

    public function editAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute(
                'comments',
                array(
                    'action' => 'reply'
                )
            );
        }

        try {
            $comment = $this->getCommentsTable()->getComment($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute(
                'comments',
                array(
                    'action' => 'index'
                )
            );
        }

        $form = new CommentForm();
        $form->bind($comment);
        $form->get('submit')->setAttribute('value', 'Edit');

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getCommentsTable()->saveComment($comment);

                return $this->redirect()->toRoute('comments');
            }
        }
        return array(
            'id'   => $id,
            'form' => $form
        );
    }

    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('comments');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id           = (int)$request->getPost('id');
                $commentTable = $this->getCommentsTable();

                if (count($commentTable->fetchChildComments($id)) > 0) {

                    $comment         = $commentTable->getComment($id);
                    $comment->author = 'deleted';
                    $comment->text   = 'deleted';
                    $commentTable->saveComment($comment);
                }
                else {
                    $commentTable->deleteComment($id);
                }
            }

            return $this->redirect()->toRoute('comments');
        }

        return array(
            'id'      => $id,
            'comment' => $this->getCommentsTable()->getComment($id)
        );
    }

    public function setCommentsTable($ct)
    {
        $this->commentsTable = $ct;
    }


    public function getCommentsTable()
    {
        if (!$this->commentsTable) {
            $sm                  = $this->getServiceLocator();
            $this->commentsTable = $sm->get('Comments\Model\CommentsTable');
        }
        return $this->commentsTable;
    }
}