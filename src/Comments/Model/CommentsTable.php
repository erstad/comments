<?php
/**
 * Created by PhpStorm.
 * User: henrik
 * Date: 16.11.15
 * Time: 11:16
 */

namespace Comments\Model;

use Zend\Db\TableGateway\TableGateway;

class CommentsTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function fetchCommentsTree()
    {
        $commentArray = $this->fetchChildComments(0);
        return $commentArray;
    }

    public function fetchChildComments($parentID)
    {

        $parentID = (int)$parentID;
        $commentArray = array();
        $rowset = $this->tableGateway->select(array('parent' => $parentID));

        foreach ($rowset as $row) {
            $currentComment = array(
                'id' => $row->id,
                'text' => $row->text,
                'author' => $row->author,
                'children' => $this->fetchChildComments($row->id)
            );

            $commentArray[] = $currentComment;
        }

        return $commentArray;
    }

    /**
     * @param $id
     *
     * @return array|\ArrayObject|Comment
     * @throws \Exception
     */
    public function getComment($id)
    {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveComment(Comment $comment)
    {
        $parent = (!$comment->parent) ? 0 : $comment->parent;

        $data = array(
            'author' => $comment->author,
            'text' => $comment->text,
            'parent' => $parent,
        );

        $id = (int) $comment->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getComment($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Comment id does not exist');
            }
        }
    }

    public function deleteComment($id)
    {
        $this->tableGateway->delete(array('id' => (int)$id));
    }
}