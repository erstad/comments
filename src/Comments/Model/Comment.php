<?php
/**
 * Created by PhpStorm.
 * User: henrik
 * Date: 16.11.15
 * Time: 11:10
 */

namespace Comments\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Comment
{
    public $id;
    public $author;
    public $text;
    public $parent;

    public function exchangeArray($data)
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->author = (!empty($data['author'])) ? $data['author'] : null;
        $this->text = (!empty($data['text'])) ? $data['text'] : null;
        $this->parent = (!empty($data['parent'])) ? $data['parent'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}