<?php
/**
 * Created by PhpStorm.
 * User: henrik
 * Date: 16.11.15
 * Time: 10:43
 */
namespace Comments;

return array(
    'controllers' => array(
        'invokables' => array(
            'Comments\Controller\Comments' => 'Comments\Controller\CommentsController'
        )
    ),
    'router' => array(
        'routes' => array(
            'comments' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/comments[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Comments\Controller\Comments',
                        'action'     => 'index',
                    )
                )
            )
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'template_map' => array(
        'view/printCommentMacro'      => __DIR__ . '/../view/printCommentMacro.twig'
    ),
);